class Solution {
    public String solution(String s) {
        String answer = "";
        int sLenth = s.length();
        if(sLenth<1 || sLenth>100){
            return null;
        }
        switch(sLenth%2){
        	case 1 :
        		answer = s.substring(sLenth/2, sLenth/2+1);
        		break;
        	case 0 :
        	default :
        		answer = s.substring(sLenth/2-1, sLenth/2+1);
        		break;
        }
        return answer;
    }
}
